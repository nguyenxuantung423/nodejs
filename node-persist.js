//Load module node-persist
var storage = require('node-persist');

//Hàm khởi tạo
//Load dữ liệu đã lưu trên ổ đĩa
storage.initSync({
	dir:'students' //cấu hình dữ liệu lưu trong thư mục studens
});

function getAllStudents(){
	var students = storage.getItemSync('students');
	if(typeof students === 'undefined'){
		return [];
	}
	return students;
}

function getStudent(studentId){
	var students = getAllStudents();
     
    // Biến lưu trữ sinh viên được tìm thấy
    var matchedStudent = null;
     
    // Lặp để tìm sinh viên
    for (var i = 0; i < students.length; i++){
        if (students[i].id === studentId){
            matchedStudent = students[i];
            break;
        }
    }
     
    return matchedStudent;
}

function addStudent(id, fullname){
	var students = getAllStudents();
     
    students.push({
        id : id,
        fullname : fullname
    });
     
    storage.setItemSync('students', students);
}

function removeStudent(studentId){
	 var students = getAllStudents();
     
    for (var i = 0; i < students.length; i++){
        if (students[i].id === studentId){
            students.splice(i, 1);
        }
    }
     
    storage.setItemSync('students', students);
}

function removeAllStudents(){
    storage.setItemSync('students', []);
}

function editStudent(studentId, studentName){
	 var students = getAllStudents();
 
    for (var i = 0; i < students.length; i++){
        if (students[i].id === studentId){
            students[i].fullname = studentName;
        }
    }
     
    storage.setItemSync('students', students);
}

function showStudents(){
	var students = getAllStudents();
    students.forEach(function(student){
        console.log('Student: ' + student.fullname + ' (' + student.id + ')');
    });
}

removeAllStudents();
addStudent(1, 'Cuong');
addStudent(2, 'Kinh');
addStudent(3, 'Chinh');
addStudent(4, 'Quyen');

 
// Hiển thị danh sách sinh viên
showStudents();